.386
.MODEL flat,STDCALL

STD_INPUT_HANDLE equ -10
STD_OUTPUT_HANDLE equ -11

GetStdHandle PROTO :DWORD
ExitProcess PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
atoi  PROTO :DWORD
.DATA
		cout		   dd ?
		cin			   dd ?
		tekst          db "Wprowadz a: ",0
		rozmiart       dd $ - tekst
		tekst2         db "Wprowadz b: ",0
		rozmiart2      dd $ - tekst2
		tekst3         db "Wprowadz c: ",0
		rozmiart3      dd $ - tekst3
		tekst4         db "Wprowadz d: ",0
		rozmiart4      dd $ - tekst4
		liczba         dd 0
		liczbaW		   db "Wynik (a+b)*c/d  to: %i ",0
		rliczbaW	   dd $ - liczbaW
		wynik          dw 128  DUP(0)
		bufor          dw 128 DUP(0)
		bufor2         dw 128 DUP(0)
		bufor3         db 2 DUP(0)
		bufor4         db 2 DUP(0)
		zm             dw ?
		zm2            dw ?
		zm3            dw ?
		zm4            dw ?
		liczbaZ        dd 0
		rozmiar		   dd ?
		teksto         db 128 DUP(0)
.CODE
main proc
	invoke GetStdHandle, STD_OUTPUT_HANDLE
	mov cout, EAX
	
	invoke GetStdHandle, STD_INPUT_HANDLE
	mov cin, EAX

	invoke WriteConsoleA, cout, OFFSET tekst, rozmiart, OFFSET liczba, 0
	invoke ReadConsoleA, cin, OFFSET bufor, 4, OFFSET liczba, 0
	LEA EBX, bufor
	mov EDI,liczba
	mov BYTE PTR [EBX+EDI-2],0
		invoke atoi, OFFSET bufor

	mov zm, AX
	invoke WriteConsoleA, cout, OFFSET tekst2, rozmiart2, OFFSET liczba, 0

	invoke ReadConsoleA, cin, OFFSET bufor, 4, OFFSET liczbaZ, 0
	LEA EBX, bufor
	mov EDI,liczbaZ
	mov BYTE PTR [EBX+EDI-2],0
	invoke atoi, OFFSET bufor
	mov zm2, AX

	invoke WriteConsoleA, cout, OFFSET tekst3, rozmiart3, OFFSET liczba, 0
	invoke ReadConsoleA, cin, OFFSET bufor, 4, OFFSET liczbaZ, 0
	LEA EBX, bufor
	mov EDI,liczbaZ
	mov BYTE PTR [EBX+EDI-2],0
	invoke atoi, OFFSET bufor
	mov zm3, AX
	
	invoke WriteConsoleA, cout, OFFSET tekst4, rozmiart4, OFFSET liczba, 0
	invoke ReadConsoleA, cin, OFFSET bufor2, 4, OFFSET liczbaZ, 0
	LEA EBX, bufor2
	mov EDI,liczbaZ
	mov BYTE PTR [EBX+EDI-2],0
	invoke atoi, OFFSET bufor2
	mov zm4, AX

	mov AX,zm
	mov BX, zm3
	mul  EBX
	mov zm,aX
	
	mov AX, zm2
	mov BX, zm3
	mul EBX
	mov zm2, aX

	mov AX, zm
	add AX, zm2

	mov BX, zm4 
	xor EDX, EDX
	div  BX
	mov zm, AX
	invoke wsprintfA, OFFSET wynik, OFFSET liczbaW, zm
	invoke WriteConsoleA, cout, OFFSET wynik, rliczbaW , OFFSET liczba, 0

	invoke ExitProcess, 0



main endp
atoi proc uses esi edx inputBuffAddr:DWORD
	mov esi, inputBuffAddr
	xor edx, edx
	xor EAX, EAX
	mov AL, BYTE PTR [esi]
	cmp eax, 2dh
	je parseNegative

	.Repeat
		
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0
	mov EAX, EDX
	jmp endatoi

	parseNegative:
	inc esi
	.Repeat
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0

	xor EAX,EAX
	sub EAX, EDX
	jmp endatoi

	endatoi:
	ret
atoi endp
END